package trie;

/**
 * Created by macbookpro on 09.05.2016.
 */
public class Trie {
    private Node root;

    public Trie() {
        root = new Node();
    }

    public void insert(String word) {
        Node current = root;

        for (int i = 0; i < word.length(); i++) {
            char currentChar = word.charAt(i);
            Node newNodeToAdd = current.descendant.get(currentChar);

            if (newNodeToAdd == null) {
                newNodeToAdd = new Node();
                current.descendant.put(currentChar, newNodeToAdd);
            }

            current = newNodeToAdd;
        }

        current.lastLetter = true;
    }

    public boolean contains(String word) {
        Node current = root;

        for (int i = 0; i < word.length(); i++) {
            char currentChar = word.charAt(i);
            Node nodeWithCurrentChar = current.descendant.get(currentChar);

            if (nodeWithCurrentChar == null) {
                return false;
            } else {
                current = nodeWithCurrentChar;
            }
        }

        return current.lastLetter;
    }

    public static void main(String args[]) {
        Trie t = new Trie();
        t.insert("abcdef");
        t.insert("abcghi");
        System.out.println(t.contains("abcghi"));
    }
}
