package trie;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by macbookpro on 10.05.2016.
 */
public class Node {
    Map<Character, Node> descendant;
    boolean lastLetter;

    public Node() {
        descendant = new HashMap<>();
        lastLetter = false;
    }
}
