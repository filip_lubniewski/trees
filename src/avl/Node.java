package avl;

/**
 * Created by macbookpro on 03.05.2016.
 */
public class Node {
    public Node right;
    public Node left;
    public int height;
    public int data;

    public static Node newNode(int data) {
        return new Node(data);
    }

    public Node(int data) {
        this.data = data;
    }
}
