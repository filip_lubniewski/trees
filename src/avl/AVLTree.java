package avl;

/**
 * Created by macbookpro on 03.05.2016.
 */
public class AVLTree {

    private int setNumberOfDescendants(Node root) {
        if (root == null) {
            return 0;
        }

        int numberOfDescendants = Math.max((root.left != null ? root.left.height : 0), (root.right != null ? root.right.height : 0));

        return 1 + numberOfDescendants;
    }

    private int numberOfDescendants(Node root) {
        if (root == null) {
            return 0;
        } else {
            return root.height;
        }
    }

    private Node leftRotate(Node root) {
        Node newRoot = root.right;
        root.right = root.right.left;
        newRoot.left = root;
        root.height = setNumberOfDescendants(root);
        newRoot.height = setNumberOfDescendants(newRoot);

        return newRoot;
    }

    private Node rightRotate(Node root) {
        Node newRoot = root.left;
        root.left = root.left.right;
        newRoot.right = root;
        root.height = setNumberOfDescendants(root);
        newRoot.height = setNumberOfDescendants(newRoot);

        return newRoot;
    }

    public Node insert(Node root, int data) {
        if (root == null) {
            Node temporaryNode = Node.newNode(data);
            temporaryNode.height = 1;

            return temporaryNode;
        }

        if (root.data <= data) {
            root.right = insert(root.right, data);
        } else {
            root.left = insert(root.left, data);
        }

        int balance = numberOfDescendants(root.left) - numberOfDescendants(root.right);

        if (balance > 1) {
            root = balanceTreeLeftToRight(root);
        } else if (balance < -1) {
            root = balanceTreeRightToLeft(root);
        } else {
            root.height = setNumberOfDescendants(root);
        }

        return root;
    }

    private Node balanceTreeRightToLeft(Node root) {
        if (numberOfDescendants(root.right.right) >= numberOfDescendants(root.right.left)) {
            root = leftRotate(root);
        } else {
            root.right = rightRotate(root.right);
            root = leftRotate(root);
        }

        return root;
    }

    private Node balanceTreeLeftToRight(Node root) {
        if (numberOfDescendants(root.left.left) >= numberOfDescendants(root.left.right)) {
            root = rightRotate(root);
        } else {
            root.left = leftRotate(root.left);
            root = rightRotate(root);
        }

        return root;
    }


    public static void main(String args[]) {
        AVLTree avlTree = new AVLTree();
        Node root = null;
        root = avlTree.insert(root, 20);
        root = avlTree.insert(root, 15);
        root = avlTree.insert(root, 0);
        root = avlTree.insert(root, 5);

        TreePrinter.printTree(root);

        root = avlTree.insert(root, 6);
        TreePrinter.printTree(root);

        root = avlTree.insert(root, 7);
        TreePrinter.printTree(root);


        root = avlTree.insert(root, 8);
        TreePrinter.printTree(root);

        root = avlTree.insert(root, 9);
        TreePrinter.printTree(root);
    }
}
