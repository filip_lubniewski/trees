package avl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbookpro on 11.05.2016.
 */

// Rozwiązanie drukowania struktury drzewa zaczerpnięte z http://stackoverflow.com/a/4973083

public class TreePrinter {
    public static void printTree(Node root) {
        int depthOfTree = root.height;
        List<Node> nodes = new ArrayList<>();
        nodes.add(root);

        printNodes(nodes, 1, depthOfTree);
    }

    public static void printNodes(List<Node> nodes, int currentLevel, int depthOfTree) {
        if (nodes.isEmpty() || isAllElementsNull(nodes)) return;

        int floor = depthOfTree - currentLevel;
        int edgeLines = (int) Math.pow(2, (Math.max(floor - 1, 0)));
        int firstSpaces = (int) Math.pow(2, (floor)) - 1;
        int betweenSpaces = (int) Math.pow(2, (floor + 1)) - 1;

        if (nodes.get(0) != null) {
            if (nodes.get(0).data < 0)
                printWhiteSpaces(firstSpaces - 1);
            else
                printWhiteSpaces(firstSpaces);
        }

        List<Node> descendants = new ArrayList<>();
        for (Node node : nodes) {
            if (node != null) {
                System.out.print(node.data);
                descendants.add(node.left);
                descendants.add(node.right);
            } else {
                descendants.add(null);
                descendants.add(null);
                System.out.print(" ");
            }

            if (node != null) {
            }
            printWhiteSpaces(betweenSpaces);
        }

        System.out.println("");

        for (int i = 1; i <= edgeLines; i++) {
            for (int j = 0; j < nodes.size(); j++) {
                printWhiteSpaces(firstSpaces - i);

                if (nodes.get(j) == null) {
                    printWhiteSpaces(edgeLines + edgeLines + i + 1);
                    continue;
                }

                if (nodes.get(j).left != null)
                    System.out.print("/");
                else
                    printWhiteSpaces(1);

                printWhiteSpaces(i + i - 1);

                if (nodes.get(j).right != null)
                    System.out.print("\\");
                else
                    printWhiteSpaces(1);

                printWhiteSpaces(edgeLines + edgeLines - i);
            }

            System.out.println("");
        }

        printNodes(descendants, currentLevel + 1, depthOfTree);
    }

    public static void printWhiteSpaces(int firstSpaces) {
        for (int i = 0; i < firstSpaces; i++)
            System.out.print(" ");
    }

    public static boolean isAllElementsNull(List<Node> nodes) {
        for (Object object : nodes) {
            if (object != null) return false;
        }

        return true;
    }
}
